package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dominio.Contato;


@WebServlet("interna/doCadastro")
public class CadastroServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	// Lista de contatos cadastrados
	private List<Contato> contatos;
       
    public CadastroServlet() {
        super();
        contatos = new ArrayList<Contato>();
    }

    @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
		throws ServletException, IOException {
		
		// Informacoes provenientes de formulario via requisicao HTTP POST
		String nome = request.getParameter("nome");
		String email = request.getParameter("email");
		String telefone = request.getParameter("telefone");
		
		// Criacao de objeto Contato e adicao a lista de contatos
		Contato c = new Contato(nome, email, telefone);
		contatos.add(c);

		// Armazenamento da lista de contatos em objeto de sessao
		request.getSession().setAttribute("listaContatos", contatos);

		// Resposta para usuario
		String mensagem = "<html>" +
			"<body>" +
			"<h2>Opera��o realizada com sucesso</h2>" +
			"Contatos cadastrados:<br/>";
		for (Contato ct : contatos) {
			mensagem += ct.getNome() + "<br/>";
		}
		mensagem += "<br/>Clique <a href=\"http://localhost:8080/Contatos/interna/cadastro.html\">aqui</a> "+
			"para cadastrar um novo contato" +
			"</body>" +
			"</html>";
				
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println(mensagem);
	}

}
